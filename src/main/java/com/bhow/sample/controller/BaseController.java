package com.bhow.sample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BaseController {
	
	private String version = "Sample application";

	@RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
	public String welcome(ModelMap model) {

		model.addAttribute("message", version);
		return "index";

	}

	@RequestMapping(value = "/welcome/{name}", method = RequestMethod.GET)
	public String welcomeName(@PathVariable String name, ModelMap model) {

		model.addAttribute("message", version + ". " + "Hello " + name + "!");
		return "index";

	}

}
